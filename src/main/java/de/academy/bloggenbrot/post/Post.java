package de.academy.bloggenbrot.post;

import de.academy.bloggenbrot.comment.Comment;
import de.academy.bloggenbrot.user.User;

import javax.persistence.*;
import java.util.List;

@Entity                           //als Tabelle makiert.
public class Post {
    /**************************************************
     * Attribute
     **************************************************/

    @Id                          //Markiert als Primerkey
    @GeneratedValue              //Spezifiziert den Primerkey
    private long id;             //Speichert die id

    @OneToMany(mappedBy = "post")
    private List<Comment> comments;

    @ManyToOne                   //Ein User kann mehrere Posts machen
    private User user;           //Erstellt einen User

    private String text;         //Erstellt einen Text
    /**************************************************
     * Konstruktoren
     **************************************************/
    public Post(){

    }//#Standart

    public Post(String text) {
        this.text = text;

    } //Konstruktor

    /**************************************************
     * Getter only
     **************************************************/
    public long getId() {
        return id;
    }
    public String getText() {
        return text;
    }
    public List<Comment> getComments() {
        return comments;
    }

}
