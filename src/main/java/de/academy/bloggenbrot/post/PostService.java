package de.academy.bloggenbrot.post;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    private PostRepository postRepository;

    @Autowired
    public PostService(PostRepository postRepository) {

        this.postRepository = postRepository;
    }

    public List<Post> getPosts() {
        return postRepository.findAllByOrderByIdDesc();
    }


    public Optional<Post> getPost(long id) {
        return postRepository.findById(id);
    }
}


