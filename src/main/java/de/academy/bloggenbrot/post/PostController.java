package de.academy.bloggenbrot.post;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;

@Controller                     //makiert die Klasse als Controller für die automatische Erkennung
public class PostController {
    private PostService postService;

    @Autowired
    public PostController(PostService postService) {
       this.postService = postService;
    }

    /**************************************************
     * Methoden
     **************************************************/
    @GetMapping("/")
    public String postIndex(Model model){
        model.addAttribute("Postlist", postService.getPosts());
        return "home";
    }

    @PostMapping("/")
    public String index1(Model model) {
        model.addAttribute("Postlist", postService.getPosts());
        return "home";
    }
}
