package de.academy.bloggenbrot.post;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository                         //Markiert das Interface als Repository DAO
public interface PostRepository extends CrudRepository<Post, Long> /*Create,Read,Update,Delete*/ {
//    List<Post> findAll();
    List<Post> findAllByOrderByIdDesc(); //Die Liste der Posts ist zeitlich in chronologischer Reinfolge (Ascending)

}
