package de.academy.bloggenbrot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BloggenbrotApplication {

	public static void main(String[] args) {
		SpringApplication.run(BloggenbrotApplication.class, args);
	}

}
