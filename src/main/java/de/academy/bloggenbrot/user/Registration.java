package de.academy.bloggenbrot.user;

import javax.validation.constraints.NotEmpty;

public class Registration {

    private String username;
    private String password;


    public Registration(String username, String password) {
        this.username = username;
        this.password = password;

    }
    //Getter

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }


}
