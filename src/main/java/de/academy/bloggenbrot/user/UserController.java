package de.academy.bloggenbrot.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController {
    private UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("reg", new Registration("", ""));
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute("registration") Registration registration) {
        User user = new User(registration.getUsername(), registration.getPassword());
        userRepository.save(user);
        return "redirect:/";
    }
}
