package de.academy.bloggenbrot.user;

import de.academy.bloggenbrot.post.Post;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class User {

    @Id
    @GeneratedValue
    private long id;
    private String username;
    private String password;

    // posts and comments Listen
    @OneToMany //(mappedBy = "user")
    private List<Post> posts;
    // private List<Comment> comments;


    // StandartKonsturktor weil Entität
    public User() {
    }

//Konstruktor

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }


//Getter
    public List<Post> getPost() {
        return posts;
    }

}
