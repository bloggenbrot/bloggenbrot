package de.academy.bloggenbrot.comment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository                         //Markiert das Interface als Repository DAO
public interface CommentRepository extends CrudRepository<Comment, Long> /*Create,Read,Update,Delite*/ {
    List<Comment> findAllByOrderById(); //Die Liste der Posts ist zeitlich in chronologischer Reinfolge (Ascending)

}

