package de.academy.bloggenbrot.comment;

import de.academy.bloggenbrot.post.Post;
import de.academy.bloggenbrot.user.User;

import javax.persistence.*;

@Entity // tabelle für die Kommentare
public class Comment {
    @Id
    @GeneratedValue
    private long id;
    private String text;
    @ManyToOne
    private User user;
    @ManyToOne
    private Post post;

    //Konstruktoren
//Standart
    public Comment() {
    }

    public Comment(String text, User user, Post post) {
        this.text = text;
        this.user = user;
        this.post = post;
    }

    //Getter


    public long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
