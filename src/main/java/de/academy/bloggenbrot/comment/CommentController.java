package de.academy.bloggenbrot.comment;


import de.academy.bloggenbrot.post.Post;
import de.academy.bloggenbrot.post.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller                     //makiert die Klasse als Controller für die automatische Erkennung
public class CommentController {
    private PostService postService;

    @Autowired
    public CommentController(PostService postService) {
        this.postService = postService;
    }


    /**************************************************
     * Methoden
     **************************************************/
    @GetMapping("/comment")
    public String index(Model model, @RequestParam int id){

        Optional<Post> optionalPost = postService.getPost(id);
        if (!optionalPost.isPresent()){
            return "redirect:/";
        }
        Post post = optionalPost.get();
        model.addAttribute("post", post);
        return "commentPage";
    }

}

