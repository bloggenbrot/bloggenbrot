package de.academy.bloggenbrot.comment;


import de.academy.bloggenbrot.post.Post;
import de.academy.bloggenbrot.post.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    private CommentRepository commentRepository;
    private PostRepository postRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository, PostRepository postRepository) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }

    public void add(Comment comment, Long postId) {
        Optional<Post> post = postRepository.findById(postId);
        comment.setPost(post.get());
        commentRepository.save(comment);
    }

    public List<Comment> getComment() {
        return commentRepository.findAllByOrderById();
    }


}

